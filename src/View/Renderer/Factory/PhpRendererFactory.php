<?php

namespace Smtm\Mvc\View\Renderer\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Mvc\View\Renderer\PhpRenderer;
use Smtm\Psr\Container\Factory\FactoryInterface;

class PhpRendererFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $configView = $container->get('config')['templates'] ?? [];
        return new PhpRenderer($configView);
    }
}
