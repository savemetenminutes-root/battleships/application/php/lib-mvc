<?php

namespace Smtm\Mvc\View\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Mvc\View\Factory\Exception\MissingResolverException;
use Smtm\Mvc\View\Renderer\PhpRenderer;
use Smtm\Mvc\View\Resolver\AggregateResolver;
use Smtm\Mvc\View\Resolver\NamespacedPathStackResolver;
use Smtm\Mvc\View\ViewTemplateDecorator;
use Smtm\Psr\Container\Factory\FactoryInterface;

class ViewTemplateDecoratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $configView = $container->get('config')['templates'] ?? [];
        $view       = new ViewTemplateDecorator($configView);
        $layout     = array_key_exists('layout', $configView) ? $configView['layout'] : 'default';
        $resolver   = null;
        $renderer   = null;
        if(array_key_exists('resolver', $configView) && class_exists($configView['resolver'])) {
            if($container->has($configView['resolver'])) {
                $resolver = $container->get($configView['resolver']);
            } else {
                $resolver = new $configView['resolver']();
            }
        } else {
            $resolver = $container->get(NamespacedPathStackResolver::class);
        }
        if(array_key_exists('renderer', $configView) && class_exists($configView['renderer'])) {
            if($container->has($configView['renderer'])) {
                $renderer = $container->get($configView['renderer']);
            } else {
                $renderer = new $configView['renderer']();
            }
        } else {
            $renderer = $container->get(PhpRenderer::class);
        }
        $view
            ->setResolver($resolver)
            ->setRenderer($renderer)
            ->setLayout($layout);

        return $view;
    }
}
