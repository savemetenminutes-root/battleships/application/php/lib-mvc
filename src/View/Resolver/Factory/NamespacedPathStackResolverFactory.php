<?php

namespace Smtm\Mvc\View\Resolver\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Mvc\View\Resolver\NamespacedPathStackResolver;
use Smtm\Psr\Container\Factory\FactoryInterface;

class NamespacedPathStackResolverFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $configTemplatePaths = $container->get('config')['templates']['paths'] ?? [];
        return new NamespacedPathStackResolver($configTemplatePaths);
    }
}
