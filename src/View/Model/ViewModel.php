<?php

namespace Smtm\Mvc\View\Model;

class ViewModel implements ModelInterface
{
    protected $variables;

    public function __construct(array $variables = [])
    {
        $this->variables = $variables;
    }

    public function addChild(ModelInterface $child, $captureTo = null, $append = false)
    {
        // TODO: Implement addChild() method.
    }

    public function captureTo()
    {
        // TODO: Implement captureTo() method.
    }

    public function getChildren()
    {
        // TODO: Implement getChildren() method.
    }

    public function getOptions()
    {
        // TODO: Implement getOptions() method.
    }

    public function getTemplate()
    {
        // TODO: Implement getTemplate() method.
    }

    public function getVariable($name, $default = null)
    {
        // TODO: Implement getVariable() method.
    }

    public function getVariables()
    {
        // TODO: Implement getVariables() method.
    }

    public function hasChildren()
    {
        // TODO: Implement hasChildren() method.
    }

    public function isAppend()
    {
        // TODO: Implement isAppend() method.
    }

    public function setAppend($append)
    {
        // TODO: Implement setAppend() method.
    }

    public function setCaptureTo($capture)
    {
        // TODO: Implement setCaptureTo() method.
    }

    public function setOption($name, $value)
    {
        // TODO: Implement setOption() method.
    }

    public function setOptions($options)
    {
        // TODO: Implement setOptions() method.
    }

    public function setTemplate($template)
    {
        // TODO: Implement setTemplate() method.
    }

    public function setTerminal($terminate)
    {
        // TODO: Implement setTerminal() method.
    }

    public function setVariable($name, $value)
    {
        // TODO: Implement setVariable() method.
    }

    public function setVariables($variables)
    {
        // TODO: Implement setVariables() method.
    }

    public function terminate()
    {
        // TODO: Implement terminate() method.
    }

    public function count()
    {
        // TODO: Implement count() method.
    }

    public function getIterator()
    {
        // TODO: Implement getIterator() method.
    }
}
