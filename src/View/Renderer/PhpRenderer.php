<?php

namespace Smtm\Mvc\View\Renderer;

class PhpRenderer implements RendererInterface
{
    public function render($model, $template): string
    {
        extract($model->getVariables());
        ob_start();
        include $template;
        return ob_get_clean();
    }
}
