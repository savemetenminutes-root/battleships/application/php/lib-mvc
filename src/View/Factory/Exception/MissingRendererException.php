<?php

declare(strict_types=1);

namespace Smtm\Mvc\View\Factory\Exception;

use RuntimeException;

class MissingRendererException extends RuntimeException
{
}
