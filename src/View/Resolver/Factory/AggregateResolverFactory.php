<?php

namespace Smtm\Mvc\View\Resolver\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Mvc\View\Resolver\AggregateResolver;
use Smtm\Psr\Container\Factory\FactoryInterface;

class AggregateResolverFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AggregateResolver();
    }
}
