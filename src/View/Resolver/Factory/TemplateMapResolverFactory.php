<?php

namespace Smtm\Mvc\View\Resolver\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Mvc\View\Resolver\TemplateMapResolver;
use Smtm\Psr\Container\Factory\FactoryInterface;

class TemplateMapResolverFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new TemplateMapResolver();
    }
}
