<?php

namespace Smtm\Mvc\View\Resolver;

use Smtm\Mvc\View\Renderer\RendererInterface as Renderer;
use SplFileInfo;
use SplStack;

class NamespacedPathStackResolver extends TemplatePathStackResolver
{
    public const DEFAULT_NAMESPACE = '__DEFAULT__';

    /**
     * @var array
     */
    protected $paths = [];

    public function __construct($configTemplatePaths)
    {
        // Add template paths
        foreach ($configTemplatePaths as $namespace => $paths) {
            $namespace = is_numeric($namespace) ? null : $namespace;
            foreach ((array) $paths as $path) {
                $this->addPath($path, $namespace);
            }
        }
    }

    /**
     * Add a path to the stack with the given namespace.
     *
     * @param string $path
     * @param string|null $namespace
     */
    public function addPath($path, ?string $namespace = self::DEFAULT_NAMESPACE) : void
    {
        if (! is_string($path)) {
            throw new ViewException\InvalidArgumentException(sprintf(
                'Invalid path provided; expected a string, received %s',
                gettype($path)
            ));
        }

        if (null === $namespace) {
            $namespace = self::DEFAULT_NAMESPACE;
        }

        if (! is_string($namespace) || empty($namespace)) {
            throw new ViewException\InvalidArgumentException(
                'Invalid namespace provided; must be a non-empty string'
            );
        }

        if (! array_key_exists($namespace, $this->paths)) {
            $this->paths[$namespace] = new SplStack();
        }

        $this->paths[$namespace]->push(static::normalizePath($path));
    }

    public function resolve($name, Renderer $renderer = null)
    {
        $namespace = self::DEFAULT_NAMESPACE;
        $template  = $name;
        if (preg_match('#^(?P<namespace>[^:]+)::(?P<template>.*)$#', $template, $matches)) {
            $namespace = $matches['namespace'];
            $template  = $matches['template'];
        }

//        $this->lastLookupFailure = false;
//
//        if ($this->isLfiProtectionOn() && preg_match('#\.\.[\\\/]#', $template)) {
//            throw new ViewException\DomainException(
//                'Requested scripts may not include parent directory traversal ("../", "..\\" notation)'
//            );
//        }
//
//        if (! count($this->paths)) {
//            $this->lastLookupFailure = static::FAILURE_NO_PATHS;
//            return null;
//        }

        // Ensure we have the expected file extension
        $defaultSuffix = $this->getDefaultSuffix();
        if (pathinfo($template, PATHINFO_EXTENSION) == '') {
            $template .= '.' . $defaultSuffix;
        }

        $path = false;
        if ($namespace !== self::DEFAULT_NAMESPACE) {
            $path = $this->getPathFromNamespace($template, $namespace);
        }

        $path = $path ?: $this->getPathFromNamespace($template, self::DEFAULT_NAMESPACE);

        if ($path) {
            return $path;
        }

//        $this->lastLookupFailure = static::FAILURE_NOT_FOUND;
//        return null;
    }

    /**
     * Fetch a template path from a given namespace.
     *
     * @return null|string String path on success; null on failure
     */
    private function getPathFromNamespace(string $template, string $namespace) : ?string
    {
        if (! array_key_exists($namespace, $this->paths)) {
            return null;
        }

        foreach ($this->paths[$namespace] as $path) {
            $file = new SplFileInfo($path . $template);
            if ($file->isReadable()) {
                // Found! Return it.
                if (($filePath = $file->getRealPath()) === false && substr($path, 0, 7) === 'phar://') {
                    // Do not try to expand phar paths (realpath + phars == fail)
                    $filePath = $path . $template;
                    if (! file_exists($filePath)) {
                        break;
                    }
                }

//                if ($this->useStreamWrapper()) {
//                    // If using a stream wrapper, prepend the spec to the path
//                    $filePath = 'zend.view://' . $filePath;
//                }
                return $filePath;
            }
        }

        return null;
    }
}
