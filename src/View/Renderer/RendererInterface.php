<?php

namespace Smtm\Mvc\View\Renderer;

/**
 * Interface class for Smtm\Mvc\View\Renderer\* compatible template engine implementations
 */
interface RendererInterface
{
    /**
     * Processes a view script and returns the output.
     *
     * @param $model
     * @param $template
     * @return string The script output.
     */
    public function render($model, $template): string;
}
