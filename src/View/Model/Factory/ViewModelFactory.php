<?php

namespace Smtm\Mvc\View\Model\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Mvc\View\Model\ViewModel;
use Smtm\Psr\Container\Factory\FactoryInterface;

class ViewModelFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ViewModel();
    }
}
