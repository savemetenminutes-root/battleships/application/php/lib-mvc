<?php

namespace Smtm\Mvc\View;

use Smtm\Mvc\View\Model\ModelInterface;

class ViewTemplateDecorator extends AbstractView
{
    public function renderModel(ModelInterface $model): string
    {
        $template = $this->resolver->resolve();
        return $this->renderer->render($model, $template);
    }
}
