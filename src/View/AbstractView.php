<?php

namespace Smtm\Mvc\View;

use Smtm\Mvc\View\Model\ModelInterface;
use Smtm\Mvc\View\Renderer\RendererInterface;

abstract class AbstractView
{
    protected $config;
    protected $model;
    protected $resolver;
    protected $renderer;
    protected $layout;

    public function __construct(array $config = [], $model = null)
    {
        $this->config = $config;
        $this->model  = $model;
    }

    abstract public function renderModel(
        ModelInterface $model
//        ModelInterface $model,
//        RendererInterface $renderer,
//        ModelInterface $root = null
    ): string;

    /**
     * @return null
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param null $model
     * @return AbstractView
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getResolver()
    {
        return $this->resolver;
    }

    /**
     * @param mixed|string $resolver
     * @return AbstractView
     */
    public function setResolver($resolver)
    {
        $this->resolver = $resolver;
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param mixed|string $renderer
     * @return AbstractView
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed|string $layout
     * @return AbstractView
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }
}
