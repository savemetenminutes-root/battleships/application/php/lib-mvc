<?php

namespace Smtm\Mvc\View\Resolver;

use Smtm\Mvc\View\Renderer\RendererInterface as Renderer;
use SplFileInfo;
use SplStack;

class TemplatePathStackResolver implements ResolverInterface
{
    /**
     * Default suffix to use
     *
     * Appends this suffix if the template requested does not use it.
     *
     * @var string
     */
    protected $defaultSuffix = 'phtml';
    /**
     * @var SplStack
     */
    protected $paths;

    public function __construct($configTemplatePaths = [])
    {
        $this->paths = new SplStack;
    }

    /**
     * Normalize a path for insertion in the stack
     *
     * @param  string $path
     * @return string
     */
    public static function normalizePath($path)
    {
        $path = rtrim($path, '/');
        $path = rtrim($path, '\\');
        $path .= DIRECTORY_SEPARATOR;
        return $path;
    }

    /**
     * Set default file suffix
     *
     * @param  string $defaultSuffix
     * @return TemplatePathStackResolver
     */
    public function setDefaultSuffix($defaultSuffix)
    {
        $this->defaultSuffix = (string) $defaultSuffix;
        $this->defaultSuffix = ltrim($this->defaultSuffix, '.');
        return $this;
    }

    /**
     * Get default file suffix
     *
     * @return string
     */
    public function getDefaultSuffix()
    {
        return $this->defaultSuffix;
    }

    /**
     * Add many paths to the stack at once
     *
     * @param  array $paths
     * @return TemplatePathStackResolver
     */
    public function addPaths(array $paths)
    {
        foreach ($paths as $path) {
            $this->addPath($path);
        }
        return $this;
    }

    /**
     * Rest the path stack to the paths provided
     *
     * @param  SplStack|array $paths
     * @return TemplatePathStackResolver
     * @throws Exception\InvalidArgumentException
     */
    public function setPaths($paths)
    {
        if ($paths instanceof SplStack) {
            $this->paths = $paths;
        } elseif (is_array($paths)) {
            $this->clearPaths();
            $this->addPaths($paths);
        } else {
            throw new Exception\InvalidArgumentException(
                "Invalid argument provided for \$paths, expecting either an array or SplStack object"
            );
        }

        return $this;
    }

    /**
     * Add a single path to the stack
     *
     * @param  string $path
     * @return TemplatePathStackResolver
     * @throws Exception\InvalidArgumentException
     */
    public function addPath($path)
    {
        if (! is_string($path)) {
            throw new Exception\InvalidArgumentException(sprintf(
                'Invalid path provided; must be a string, received %s',
                gettype($path)
            ));
        }
        $this->paths[] = static::normalizePath($path);
        return $this;
    }

    public function resolve($name, Renderer $renderer = null)
    {
//        $this->lastLookupFailure = false;
//
//        if ($this->isLfiProtectionOn() && preg_match('#\.\.[\\\/]#', $name)) {
//            throw new Exception\DomainException(
//                'Requested scripts may not include parent directory traversal ("../", "..\\" notation)'
//            );
//        }
//
//        if (! count($this->paths)) {
//            $this->lastLookupFailure = static::FAILURE_NO_PATHS;
//            return false;
//        }

        // Ensure we have the expected file extension
        $defaultSuffix = $this->getDefaultSuffix();
        if (pathinfo($name, PATHINFO_EXTENSION) == '') {
            $name .= '.' . $defaultSuffix;
        }

        foreach ($this->paths as $path) {
            $file = new SplFileInfo($path . $name);
            if ($file->isReadable()) {
                // Found! Return it.
                if (($filePath = $file->getRealPath()) === false && 0 === strpos($path, 'phar://')) {
                    // Do not try to expand phar paths (realpath + phars == fail)
                    $filePath = $path . $name;
                    if (! file_exists($filePath)) {
                        break;
                    }
                }
//                if ($this->useStreamWrapper()) {
//                    // If using a stream wrapper, prepend the spec to the path
//                    $filePath = 'zend.view://' . $filePath;
//                }
                return $filePath;
            }
        }

//        $this->lastLookupFailure = static::FAILURE_NOT_FOUND;
//        return false;
    }
}
