<?php

namespace Smtm\Mvc\View\Resolver;

use Countable;
use IteratorAggregate;
use Smtm\Mvc\View\Renderer\RendererInterface as Renderer;

class AggregateResolver implements Countable, IteratorAggregate, ResolverInterface
{
    public function count()
    {
        // TODO: Implement count() method.
    }

    public function getIterator()
    {
        // TODO: Implement getIterator() method.
    }

    public function resolve($name, Renderer $renderer = null)
    {
        // TODO: Implement resolve() method.
    }
}
