<?php

declare(strict_types=1);

namespace Smtm\Mvc\View;

use Smtm\Mvc\View\Factory\ViewTemplateDecoratorFactory;
use Smtm\Mvc\View\Model\Factory\ViewModelFactory;
use Smtm\Mvc\View\Model\ViewModel;
use Smtm\Mvc\View\Renderer\Factory\PhpRendererFactory;
use Smtm\Mvc\View\Renderer\PhpRenderer;
use Smtm\Mvc\View\Resolver\AggregateResolver;
use Smtm\Mvc\View\Resolver\Factory\AggregateResolverFactory;
use Smtm\Mvc\View\Resolver\Factory\NamespacedPathStackResolverFactory;
use Smtm\Mvc\View\Resolver\NamespacedPathStackResolver;

/**
 * The configuration provider for the module
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'shared' => [
                'factories' => [
                    ViewTemplateDecorator::class => ViewTemplateDecoratorFactory::class,
                    ViewModel::class => ViewModelFactory::class,
                    AggregateResolver::class => AggregateResolverFactory::class,
                    NamespacedPathStackResolver::class => NamespacedPathStackResolverFactory::class,
                    PhpRenderer::class => PhpRendererFactory::class,
                ],
            ],
        ];
    }
}
